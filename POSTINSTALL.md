Omeka S is pre-setup with an admin account. Go to the Admin dashboard and use this initial credentials:

- Email: admin@example.com 
- Password: changeme

**Note:** Please change the email and the password upon first login. 

Themes and modules can be installed via the terminal or using the Cloudron CLI tools. Take care to verify compatibility withe Omeka S version and preinstalled components with Cloudron Omeka S app.
