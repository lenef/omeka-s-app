<?php
return [
    'logger' => [
        'log' => false,
        'priority' => \Zend\Log\Logger::NOTICE,
    ],
    'http_client' => [
        'sslcapath' => null,
        'sslcafile' => null,
    ],
    'cli' => [
        'phpcli_path' => null,
    ],
    'thumbnails' => [
        'types' => [
            'large' => ['constraint' => 800],
            'medium' => ['constraint' => 200],
            'square' => ['constraint' => 200],
        ],
        'thumbnailer_options' => [
            'imagemagick_dir' => null,
        ],
    ],
    'translator' => [
        'locale' => 'en_US',
    ],
    'service_manager' => [
        'aliases' => [
            'Omeka\File\Store' => 'Omeka\File\Store\Local',
            'Omeka\File\Thumbnailer' => 'Omeka\File\Thumbnailer\ImageMagick',
        ],
    ],
    'mail' => [
        'transport' => [
            'type' => 'smtp',
            'options' => [
                'name' =>  getenv('CLOUDRON_MAIL_SMTP_SERVER'),
                'host' => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
                'port' => getenv('CLOUDRON_MAIL_SMTP_PORT'),
                'connection_class' => 'login',
                'connection_config' => [
                    'username' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
                    'password' => getenv('CLOUDRON_MAIL_SMTP_PASSWORD'),
                    'ssl' => null,
                    'use_complete_quit' => true,
                ],
            ],
        ],
    ],
    'ldap' => [
        'adapter_options' => [
            'server1' => [
                'host' => getenv('CLOUDRON_LDAP_SERVER'),
                'port' => getenv('CLOUDRON_LDAP_PORT'),
                'username' => getenv('CLOUDRON_LDAP_BIND_DN'),
                'password' => getenv('CLOUDRON_LDAP_BIND_PASSWORD'),
                'bindRequiresDn' => true,
                'baseDn' => getenv('CLOUDRON_LDAP_USERS_BASE_DN'),
                'accountFilterFormat' => '(&(objectclass=user)(uid=%s))',
                'accountCanonicalForm' => 4,
                'accountDomainName' => getenv('CLOUDRON_LDAP_HOST'),
            ],
        ],
    ],
];
