#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);


    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'admin@example.com';
    const PASSWORD = 'changeme';
    const TITLE = 'title';
    const DESCRIPTION = 'description';

    var browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function login(callback) {
        browser.get(`https://${app.fqdn}/login`).then(function () {
            return browser.findElement(By.id('email')).sendKeys(EMAIL);
        }).then(function () {
            return browser.findElement(By.id('password')).sendKeys(PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//div/h1/span[text()="Admin dashboard"]'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get(`https://${app.fqdn}/admin`).then(function () {
            return waitForElement(By.xpath('//div/h1/span[text()="Admin dashboard"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[text()="Logout"]')).click();
        }).then(function () {
            return waitForElement(By.id('password'));
        }).then(function () {
            callback();
        });
    }

    function createItem(callback) {
        browser.get(`https://${app.fqdn}/admin/item`).then(function () {
            return waitForElement(By.xpath('//div/h1/span[text()="Items"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[text()="Add new item"]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div/h1/span[text()="New item"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//div[@data-property-term="dcterms:title"]//textarea')).sendKeys(TITLE);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@data-property-term="dcterms:description"]//textarea')).sendKeys(DESCRIPTION);
        }).then(function () {
            return browser.findElement(By.xpath('//button[@name="add-item-submit"]')).submit();
        }).then(function () {
            return waitForElement(By.id('content'));
        }).then(function () {
            callback();
        });
    }

    function itemExists(callback) {
        browser.get(`https://${app.fqdn}/admin/item`).then(function () {
            return waitForElement(By.xpath(`//a[@class="resource-link"]//span[text()="${TITLE}"]`));
        }).then(function () {
            return browser.findElement(By.xpath(`//a[@class="resource-link"]//span[text()="${TITLE}"]`)).click();
        }).then(function () {
            return waitForElement(By.xpath(`//div/h1/span[text()="${TITLE}"]`));
        }).then(function () {
            callback();
        });
    }

    it('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create item', createItem);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    xit('can install app', function () { execSync(`cloudron install --appstore-id net.freescout.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    xit('can get app information', getAppInfo);
    xit('can login', login);
    xit('can create item', createItem);
    xit('item exists', itemExists);
    xit('can logout', logout);

    xit('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    xit('can login', login);
    xit('item exists', itemExists);
    xit('can logout', logout);

    xit('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
