FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code

# Omeka S version
ARG VERSION=3.0.2

# Ldap module (Biblibre)
ARG LDAP_MODULE_VERSION=0.3.0

# install dependencies
RUN apt-get update && apt-get install -y php libapache2-mod-php php-ldap crudini \
    rsync \
    && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf

RUN a2enmod php7.2 rewrite

# configure mod_php
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/php/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

# download and install omeka-s
RUN wget https://github.com/omeka/omeka-s/releases/download/v${VERSION}/omeka-s-${VERSION}.zip \
    -O /tmp/omeka-s.zip && \
    unzip /tmp/omeka-s.zip -d /tmp 

RUN mv /tmp/omeka-s/files /tmp/omeka-s/files_vanilla
RUN mv /tmp/omeka-s/themes /tmp/omeka-s/themes_vanilla
RUN mv /tmp/omeka-s/modules /tmp/omeka-s/modules_vanilla

# download and install Ldap module 
RUN wget https://github.com/biblibre/omeka-s-module-Ldap/releases/download/v${LDAP_MODULE_VERSION}/Ldap-${LDAP_MODULE_VERSION}.zip \
    -O /tmp/omeka-s/modules_vanilla/Ldap.zip && \
    unzip /tmp/omeka-s/modules_vanilla/Ldap.zip -d /tmp/omeka-s/modules_vanilla/ && \
    rm /tmp/omeka-s/modules_vanilla/Ldap.zip

# move files and folders
RUN rsync -r /tmp/omeka-s/ /app/code/
RUN rm -Rf /tmp/omeka-s*

COPY database.ini /app/code/config/
COPY local.config.php /app/code/config/local.config_vanilla.php 
COPY .htaccess /app/code/

RUN rm /app/code/config/local.config.php
RUN ln -sf /app/data/config/local.config.php /app/code/config/local.config.php
RUN ln -sf /app/data/files /app/code/files
RUN ln -sf /app/data/themes /app/code/themes
RUN ln -sf /app/data/modules /app/code/modules

# add code
COPY start.sh /app/pkg/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/pkg/start.sh" ]
