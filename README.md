# Omeka S Cloudron app

This repository contains the Cloudron app package source for [Omeka S](https://omeka.org/s/).

This app is experimental and needs to be extensively tested.

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/documentation/custom-apps/tutorial/#cloudron-cli).

```
cd omeka-s-app

cloudron build
cloudron install
```

## Todo

- Use [ImageMagick](https://imagemagick.org/index.php) and possibly configure its policies.
- LDAP intégration.
