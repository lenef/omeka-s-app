This app packages Omeka S v3.0.0

Omeka S is a web publication system for researchers, universities, galleries, libraries, archives, and museums. It creates a local network of independently curated exhibits sharing a collaboratively built pool of items and their metadata. 

Omeka S is licensed under GNU General Public License v3.0.
